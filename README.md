Steps to Setup

**1. Clone the repository** 

```bash
git clone https://gitlab.com/NandiniSn/task2.git
```

**2. Specify the file uploads directory**

Open `src/main/resources/application.properties` file and change the property `file.upload-dir` to the path where you want the uploaded files to be stored.

```
file.upload-dir=/Users/callicoder/uploads
```

**2. Run the app using maven**

```bash
cd file-mode
mvn spring-boot:run
```

That's it! The application can be accessed at `http://localhost:8080`.

Tested the application using POSTMAN API .
Kindly downlad and install the PostMan API in your local machine.
1) open the POSTMAN API
2) select POST method
3) http://localhost:8080/uploadFile
4) select Body , form-data ==> key--> file 
5) body 
   {
     "fileName":"Horse.png",
     "fileDownloadUri":"http://localhost:8080/downloadFile/Horse.png",
     "fileType": "image/png",
     "size" : 520089
  }


6) For download File 
   ================
   Method => Get 
   http://localhost:8080/downloadFile/Horse.png